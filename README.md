<div>
    <h1>Dos duendes LP</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 andreemalerva@gmail.com
📲 2283530727
```

# Acerca del proyecto

Este proyecto esta hecho con la ayuda de tecnologías como lo son HTML, CSS, Javascript,acompañado de frameworks como *Bootstrap, jquery*, puede ser visualizado en:

[Dos Duendes DEMO:](https://dosduendes-lp.netlify.app/)

# Politícas de privacidad

```
Las imagenes, archivos css, html y adicionales son propiedad de © 2023 LYAM ANDREE CRUZ MALERVA.
```